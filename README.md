# Deno Web Development Examples

Licensed under the MIT License. See file LICENSE.

[Deno](https://deno.land/) web development example. Example include CSS, HTML, JavaScript, JSON, TypeScript.

## CLI

Run the hello world example with the following command.

    deno run hello-world.ts

## Links

* [TypeScript](https://www.typescriptlang.org/)
* [Web Development Examples](https://gitlab.com/mneifercons/javascript/examples)
